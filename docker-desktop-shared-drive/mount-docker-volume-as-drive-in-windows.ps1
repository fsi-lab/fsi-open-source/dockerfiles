# Create docker volume called work and launch Samba server on Docker host network.
docker volume create work
docker run -itd --name samba --rm --net=host -e USERID=0 -e GROUPID=0 -v work:/work dperson/samba -s "work;/work;yes;no;no;docker" -u "docker;docker"
Start-Sleep -s 10

# Mount the volume as W: drive in Windows
net use /user:docker W: \\10.0.75.2\work docker
